=== WooCommerce Sportyplanner ===
Contributors: Miika Salo
Requires at least: 5.0
Tested up to: 5.9
Stable tag: 1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Connect your WooCommerce store with Sportyplanner. Give clients access to your excercise programs automatically.

== Changelog ==

= 1.1 =
* Tested for 5.9

= 1.0 =
* Initial release
