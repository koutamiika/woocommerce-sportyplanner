<?php
/**
 * Admin functionality.
 *
 * Adds settings menu for the plugin.
 *
 * @package woocommerce-sportyplanner
 */

/**
 * WooCommece Sportyplanner Admin class
 */
class WooCommerce_Sportyplanner_Admin {

	/**
	 * Class constructor
	 */
	public function __construct() {
		add_action( 'admin_init', array( $this, 'register_settings' ) );
		add_action( 'admin_menu', array( $this, 'register_menu_page' ) );

		add_filter( 'woocommerce_product_data_tabs', array( $this, 'add_sportyplanner_data_tab' ) );
		add_action( 'woocommerce_product_data_panels', array( $this, 'sportyplanner_data_fields' ) );
		add_action( 'woocommerce_process_product_meta', array( $this, 'sportyplanner_save_fields' ), 10, 2 );
	}

	/**
	 * Register Kouta Sportyplanner plugins menu
	 *
	 * @since 1.0.0
	 */
	public function register_menu_page() {
		add_submenu_page(
			'woocommerce',
			'Sportyplanner',
			'Sportyplanner',
			'manage_options',
			'woocommerce-sportyplanner',
			array( $this, 'load_admin_page_content' ),
			999
		);
	}

	/**
	 * Load admin menu view
	 */
	public function load_admin_page_content() {
		require_once SPORTYPLANNER_PLUGIN_DIR . '/admin/partials/woocommerce-sportyplanner-settings-view.php';
	}

	/**
	 * Register settings for the plugin
	 */
	public function register_settings() {
		register_setting(
			'woocommerce-sportyplanner-settings',
			'woocommerce-sportyplanner-api-username',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'woocommerce-sportyplanner-settings',
			'woocommerce-sportyplanner-api-password',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'woocommerce-sportyplanner-settings',
			'woocommerce-sportyplanner-api-enviroment',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
				'default'           => 'production',
			)
		);
		register_setting(
			'woocommerce-sportyplanner-settings',
			'woocommerce-sportyplanner-enable-alerts',
			array(
				'type'         => 'boolean',
				'show_in_rest' => false,
				'default'      => '1',
			)
		);
	}

	/**
	 * Add Sportyplanner tabs to product.
	 */
	public function add_sportyplanner_data_tab( $product_data_tabs ) {
		$product_data_tabs['sportifly'] = array(
			'label'  => __( 'Sportyplanner', 'sportyplanner' ),
			'target' => 'sportyplanner_data',
			'class'  => array( 'show_if_simple', 'show_if_variable' ),
		);
		return $product_data_tabs;
	}

	/**
	 * Functions you can call to output text boxes, select boxes, etc.
	 */
	public function sportyplanner_data_fields() {

		global $post;

		echo '<div id="sportyplanner_data" class="panel woocommerce_options_panel">';
		echo '<div class="options_group">';

		woocommerce_wp_text_input(
			array(
				'id'                => 'sportyplanner_id',
				'label'             => __( 'Sportyplanner ID', 'sportyplanner' ),
				'desc_tip'          => 'true',
				'description'       => __( 'Liitettävän ohjelman ID Sportyplannerissa', 'sportyplanner' ),
				'type'              => 'number',
				'custom_attributes'	=> array(
					'min'  => '1',
					'step' => '1',
				),
			)
		);

		woocommerce_wp_text_input(
			array(
				'id'          => 'trainer_email',
				'label'       => __( 'Valmentajan sähköposti', 'sportyplanner' ),
				'desc_tip'    => 'true',
				'description' => __( 'Jos tässä on arvo, niin silloin asiakassuhde solmitaan automaatisesti kun asiakas ostaa tämän tuotteen.', 'sportyplanner' ),
				'type'        => 'email',
			)
		);

		echo '</div>';
		echo '</div>';
	}

	/**
	 * Save custom data.
	 */
	public function sportyplanner_save_fields( $id, $post ) {

		if ( ! isset( $_REQUEST['_wpnonce'] ) && ! wp_verify_nonce( isset( $_REQUEST['_wpnonce'] ), 'woocommerce_meta_nonce' ) ) {
			return;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( isset( $_POST['sportyplanner_id'] ) ) {
			update_post_meta( $id, 'sportyplanner_id', (int) $_POST['sportyplanner_id'] );
		}

		if ( isset( $_POST['trainer_email'] ) ) {
			update_post_meta( $id, 'trainer_email', sanitize_email( wp_unslash( $_POST['trainer_email'] ) ) );
		}
	}
}

$admin = new WooCommerce_Sportyplanner_Admin();
