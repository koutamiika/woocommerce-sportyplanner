<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 * @package woocommerce-sportyplanner
 */

?>

<div id="poststuff" class="wrap">

	<form method="post" action="options.php">

		<?php settings_fields( 'woocommerce-sportyplanner-settings' ); ?>
		<?php do_settings_sections( 'woocommerce-sportyplanner-settings' ); ?>

		<div class="postbox">

			<div class="postbox-header">
				<h2><?php esc_html_e( 'Sportyplanner Asetukset', 'sportyplanner' ); ?></h2>
			</div>

			<div class="inside">

				<table class="form-table">

					<tr valign="top">
						<th scope="row"><label for="woocommerce-sportyplanner-api-username"><?php esc_html_e( 'Käyttäjätunnus', 'sportyplanner' ); ?><span class="required">*</span></label></th>
						<td><input type="text" class="widefat" name="woocommerce-sportyplanner-api-username" value="<?php echo esc_attr( get_option( 'woocommerce-sportyplanner-api-username' ) ); ?>" required="required"/><p class="description"><?php esc_html_e( 'Rajapinnan käyttäjätunnus', 'sportyplanner' ); ?></strong></p></td>
					</tr>

					<tr valign="top">
						<th scope="row"><label for="woocommerce-sportyplanner-api-password"><?php esc_html_e( 'Salasana', 'sportyplanner' ); ?><span class="required">*</span></label></th>
						<td><input type="password" class="widefat" name="woocommerce-sportyplanner-api-password" value="<?php echo esc_attr( get_option( 'woocommerce-sportyplanner-api-password' ) ); ?>" required="required"/><p class="description"><?php esc_html_e( 'Rajapinnan salasana', 'sportyplanner' ); ?></strong></p></td>
					</tr>

					<tr valign="top">
						<th scope="row"><label for="woocommerce-sportyplanner-api-enviroment"><?php esc_html_e( 'Rajapinnan ympäristö', 'sportyplanner' ); ?></label></th>
						<td>
							<select name="woocommerce-sportyplanner-api-enviroment" class="widefat">
								<option value="production" <?php selected( get_option( 'woocommerce-sportyplanner-api-enviroment' ), 'production' ); ?>><?php esc_html_e( 'Tuotanto', 'sportyplanner' ); ?></option>
								<option value="dev" <?php selected( get_option( 'woocommerce-sportyplanner-api-enviroment' ), 'dev' ); ?>><?php esc_html_e( 'Kehitys', 'sportyplanner' ); ?></option>
							</select>
							<p class="description"><?php esc_html_e( 'Kehitysympäristöä voidaan käyttää integraation testaukseen. Tuotantosivustoilla tulee käyttää Tuotantoympäristöä.', 'sportyplanner' ); ?></strong></p>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php esc_html_e( 'Hälytykset', 'sportyplanner' ); ?></th>
						<?php // Translators: URL to settings page. ?>
						<td><label for="woocommerce-sportyplanner-enable-alerts"><input type="checkbox" id="woocommerce-sportyplanner-enable-alerts" name="woocommerce-sportyplanner-enable-alerts" value="1" <?php checked( get_option( 'woocommerce-sportyplanner-enable-alerts' ), 1 ); ?> /><?php echo sprintf( __( 'Lähetä pääkäyttäjälle sähköposti-ilmoitus, jos automaattinen yhdistäminen Sportyplanneriin epäonnistuu. Voit määrittää vastaanottajat <a href="%s">sähköpostiasetuksista.</a>', 'sportyplanner' ), esc_url( admin_url( '/admin.php?page=wc-settings&tab=email&section=wc_email_new_order' ) ) ); ?></label></td>
					</tr>

				</table>

			</div>

		</div>

		<?php submit_button(); ?>

	</form>

</div>
