<?php
/**
 * Plugin main class
 *
 * @package woocommerce-sportyplanner
 */

/**
 * Sportyplanner class.
 */
class WooCommerce_Sportyplanner {

	/**
	 * Username for API.
	 *
	 * @var string $api_user
	 */
	private $api_user;

	/**
	 * Password for API.
	 *
	 * @var string $api_pw
	 */
	private $api_pw;

	/**
	 * URL for API.
	 *
	 * @var string $api_url
	 */
	private $api_url;

	/**
	 * Enviroment.
	 *
	 * @var string $api_enviroment
	 */
	private $api_enviroment;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->api_user       = get_option( 'woocommerce-sportyplanner-api-username' );
		$this->api_pw         = get_option( 'woocommerce-sportyplanner-api-password' );
		$this->api_enviroment = get_option( 'woocommerce-sportyplanner-api-enviroment' );

		add_action( 'woocommerce_payment_complete', array( $this, 'sportyplanner_api_call' ) );
		add_action( 'woocommerce_order_actions', array( $this, 'sportyplanner_add_order_meta_box_action' ) );
		add_action( 'woocommerce_order_action_sportyplanner_action', array( $this, 'sportyplanner_process_order_meta_box_action' ) );
	}

	/**
	 * Send order details to Sportyplanner.
	 *
	 * @param int    $order_id ID of the order.
	 * @param string $type Type of the api call. Manual or automatic after completed order.
	 */
	public function sportyplanner_api_call( $order_id, $type = null ) {

		if ( empty( $this->api_user ) || empty( $this->api_pw ) ) {
			return;
		}

		// Order Setup Via WooCommerce.
		$order = wc_get_order( $order_id );

		// Iterate Through Items.
		$items = $order->get_items();

		$order_items = array();
		foreach ( $items as $item ) {
			$product       = $item->get_product();
			$trainer_email = get_post_meta( $product->get_id(), 'trainer_email', true );

			if ( $product->is_type( 'variation' ) ) {
				$sportyplanner_id = get_post_meta( $product->get_parent_id(), 'sportyplanner_id', true ); // Get meta from variation parent.
			} else {
				$sportyplanner_id = get_post_meta( $product->get_id(), 'sportyplanner_id', true );
			}

			if ( ! empty( $sportyplanner_id ) ) {
				$order_items[] = $sportyplanner_id;
			}
		}

		// Don't send request if no ID's has been set.
		if ( empty( $order_items ) ) {
			return;
		}

		$body = array(
			'customerEmail'        => $order->get_billing_email(),
			'programs'             => $order_items,
			'language'             => 'fi',
			'personalTrainerEmail' => $trainer_email ? $trainer_email : null,
		);

		$url = '';
		switch ( $this->api_enviroment ) {
			case 'production':
				$url = 'https://www.sportyplanner.fi/api/external/purchased';
				break;
			case 'dev':
				$url = 'http://dev.sportyplanner.fi/api/external/purchased';
				break;
			default:
				$url = 'https://www.sportyplanner.fi/api/external/purchased';
				break;
		}

		$response = wp_remote_post(
			$url,
			array(
				'headers' => array(
					'Content-Type'    => 'application/json; charset=utf-8',
					'X-Auth-External' => '',
				),
				'body'    => wp_json_encode( $body ),
			)
		);

		$response_code = wp_remote_retrieve_response_code( $response );

		if ( 200 === $response_code && ! is_wp_error( $response ) ) {
			if ( 'manual' === $type ) {
				// add the order note.
				$order->add_order_note( __( '(Manuaalinen) Tilaus luotu onnistuneesti Sportyplanneriin', 'sportyplanner' ) );

				do_action( 'sportyplanner_after_manual_success', $order_id );
			} else {
				$order->add_order_note( __( 'Tilaus luotu onnistuneesti Sportyplanneriin', 'sportyplanner' ) );

				do_action( 'sportyplanner_after_success', $order_id );
			}
		} else {
			$error = $response_code;
			// Translators: Error code.
			$order->add_order_note( sprintf( __( 'VIRHE: %s. Yhdistäminen Sportyplanner-järjestelmään ei onnistunut. Yritä uudestaan.', 'sportyplanner' ), $error ) );

			// Send email alert if option is enabled.
			if ( get_option( 'woocommerce-sportyplanner-enable-alerts' ) ) {
				$this->send_notification( $order_id, $error );
			}

			do_action( 'sportyplanner_after_failure', $order_id );
		}

	}

	/**
	 * Add a custom action to order actions select box on edit order page
	 *
	 * @param array $actions order actions array to display.
	 * @return array - updated actions
	 */
	public function sportyplanner_add_order_meta_box_action( $actions ) {
		// add "Sportyplanner" custom action.
		$actions['sportyplanner_action'] = __( 'Sportyplanner', 'sportyplanner' );
		return $actions;
	}

	/**
	 * Add an order note when custom action is clicked
	 *
	 * @param WC_Order $order WooCommerce order object.
	 */
	public function sportyplanner_process_order_meta_box_action( $order ) {

		$this->sportyplanner_api_call( $order->id, 'manual' );

	}

	/**
	 * Sends notification if Sportyplanner returns error.
	 *
	 * @param int $order_id The ID of the order.
	 * @param int $error HTTP Response code.
	 */
	public function send_notification( $order_id, $error ) {

		// Get an instance of the WC_emails Object.
		$wc_emails = WC()->mailer();

		// Get available emails notifications.
		$emails_array = $wc_emails->get_emails();

		// Get the instance of the WC_Email_New_Order Object.
		$new_order_email = $emails_array['WC_Email_New_Order'];

		// Get recipients from New Order email notification.
		$new_order_recipient = $new_order_email->recipient;
		$new_order_recipient = apply_filters( 'sportyplanner_email_recipients', $new_order_recipient );

		// Translators: Order ID.
		$subject = sprintf( __( 'Tilaus #%s. Sportyplannerin yhdistäminen epäonnistui', 'sportyplanner' ), $order_id );

		// Translators: Error code.
		$email_body = sprintf( __( 'Tapahtui virhe eikä tilausta ei voitu yhdistää Sportyplanneriin. Virhekoodi oli: %s. Yritä yhdistämistä manuaalisesti.', 'sportyplanner' ), $error );
		$body       = apply_filters( 'sportyplanner_email_body', $email_body, $order_id, $error );

		// Send email regarding failed integration.
		if ( ! empty( $new_order_recipient ) ) {
			wp_mail( $new_order_recipient, $subject, $body );
		}
	}

}
