<?php
/**
 * Plugin Name: WooCommerce Sportyplanner
 * Description: Yhdistä WooCommerce kauppasi Sportyplanner-järjestelmään.
 * Version: 1.1
 * Author: Kouta Media
 * Author URI: https://koutamedia.fi
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: sportyplanner
 * Domain Path: /languages
 *
 * @package woocommerce-sportyplanner
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'SPORTYPLANNER_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

/**
 * Require WooCommerce before activating plugin
 */
function check_wc_plugin_activation() {
	if ( is_admin() && current_user_can( 'activate_plugins' ) && ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
		add_action( 'admin_notices', 'check_wc_plugin_notice' );

		deactivate_plugins( plugin_basename( __FILE__ ) );

		if ( isset( $_GET['activate'] ) ) { //phpcs:ignore
			unset( $_GET['activate'] ); //phpcs:ignore
		}
	}
}
add_action( 'admin_init', 'check_wc_plugin_activation' );

/**
 * Callback for activation notice.
 */
function check_wc_plugin_notice() {
	?>
	<div class="error"><p><?php esc_html_e( 'WooCommerce -lisäosan tulee olla asennettuna ja aktivoituna.', 'sportyplanner' ); ?></p></div>
	<?php
}

/**
 * Check for updates.
 */

require SPORTYPLANNER_PLUGIN_DIR . '/puc/plugin-update-checker.php';
$update_checker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/koutamiika/woocommerce-sportyplanner',
	__FILE__,
	'woocommerce-sportyplanner'
);

function sportyplanner_load_plugin_textdomain() {

	load_plugin_textdomain(
		'sportyplanner',
		false,
		dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
	);

}
add_action( 'init', 'sportyplanner_load_plugin_textdomain' );

require_once SPORTYPLANNER_PLUGIN_DIR . '/admin/class-woocommerce-sportyplanner-admin.php';
require_once SPORTYPLANNER_PLUGIN_DIR . '/inc/class-woocommerce-sportyplanner.php';

/**
 * Initialize the plugin.
 */
function woocommerce_sportyplanner_init() {
	if ( class_exists( 'WooCommerce' ) ) {
		$sportyplanner = new WooCommerce_Sportyplanner();
	}
}
add_action( 'init', 'woocommerce_sportyplanner_init' );
